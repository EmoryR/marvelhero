package com.tenriseteste.herosmarvel;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class MyHeroItemRecyclerViewAdapter extends RecyclerView.Adapter<MyHeroItemRecyclerViewAdapter.ViewHolder> {

    private final List<HerosMarvelItem> mValues;
    private OnHeroItemClicked mListener;

    public MyHeroItemRecyclerViewAdapter(List<HerosMarvelItem> items, OnHeroItemClicked listener) {

        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hero_item_list, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mTextoInfo.setText(mValues.get(position).getName());
        holder.mCardView.setBackground(mValues.get(position).getImage());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.OnHeroInteraction(holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mValues != null)
            return mValues.size();
        else
            return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTextoInfo;
        public HerosMarvelItem mItem;
        public CardView mCardView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTextoInfo = (TextView) view.findViewById(R.id.info_text);
            mCardView = (CardView) view.findViewById(R.id.card_view);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    public interface OnHeroItemClicked {
        void OnHeroInteraction(HerosMarvelItem hero);
    }
}
