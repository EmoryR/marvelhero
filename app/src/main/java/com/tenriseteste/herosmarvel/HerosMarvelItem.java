package com.tenriseteste.herosmarvel;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.tenriseteste.herosmarvel.util.HeroDataClass;

import java.util.List;

/**
 * Created by emoryfreitas on 07/07/16.
 */
public class HerosMarvelItem {
    @Override
    public String toString() {
        return "HerosMarvelItem{" +
                "descricao='" + descricao + '\'' +
                ", idCharacter='" + idCharacter + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    private Drawable image;
    private String descricao;
    private String idCharacter;
    private String name;
    private List<HerosMarvelItem> series;
    private List<HerosMarvelItem> comics;
    private List<HerosMarvelItem> stories;
    private List<HerosMarvelItem> events;

    public List<HerosMarvelItem> getSeries() {
        return series;
    }

    public void setSeries(List<HerosMarvelItem> series) {
        this.series = series;
    }

    public List<HerosMarvelItem> getComics() {
        return comics;
    }

    public void setComics(List<HerosMarvelItem> comics) {
        this.comics = comics;
    }

    public List<HerosMarvelItem> getStories() {
        return stories;
    }

    public void setStories(List<HerosMarvelItem> stories) {
        this.stories = stories;
    }

    public List<HerosMarvelItem> getEvents() {
        return events;
    }

    public void setEvents(List<HerosMarvelItem> events) {
        this.events = events;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getIdCharacter() {
        return idCharacter;
    }

    public void setIdCharacter(String idCharacter) {
        this.idCharacter = idCharacter;
    }
}
