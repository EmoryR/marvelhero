package com.tenriseteste.herosmarvel;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import com.tenriseteste.herosmarvel.util.HeroDataClass;
import com.tenriseteste.herosmarvel.util.HeroDataListClass;
import com.tenriseteste.herosmarvel.util.MarvelConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emoryfreitas on 07/07/16.
 */
public class GetHerosMarvelInfo extends AsyncTask<Void,Void,List<HerosMarvelItem>> {
    private Context mContext;
    // Required initialization

    private final String URL_MARVEL = "http://gateway.marvel.com/v1/public/characters?";
    WeakReference<HomeActivity> weakHome;
    private String Content;
    private String Error = null;

    public GetHerosMarvelInfo(Context context, WeakReference<HomeActivity> activity) {
        this.mContext = context;
        this.weakHome = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected List<HerosMarvelItem> doInBackground(Void... params) {
        try {
            return parseJsonMarvel(MarvelConnection.getJSONMarvel(URL_MARVEL));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<HerosMarvelItem> finalList) {
        if (weakHome != null) {
            weakHome.get().updateView(finalList);
        }
        super.onPostExecute(finalList);
    }

    public List<HerosMarvelItem> parseJsonMarvel (String json) throws JSONException {
        JSONObject jsonObject = (JSONObject) new JSONTokener(json).nextValue();
        return parseHeros(jsonObject.getJSONObject("data"));
    }

    public List<HerosMarvelItem> parseHeros(JSONObject results) throws JSONException {
        List<HerosMarvelItem> listheros  = new ArrayList<HerosMarvelItem>();
        JSONArray jsonResults = results.getJSONArray("results");
        for (int i = 0; i < jsonResults.length(); i++) {
            HerosMarvelItem hero = new HerosMarvelItem();
            JSONObject result = jsonResults.getJSONObject(i);
            hero.setIdCharacter(result.getString("id"));
            hero.setDescricao(result.getString("description"));
            hero.setName(result.getString("name"));
            JSONObject imageObject = result.getJSONObject("thumbnail");
            String urlImage = imageObject.getString("path") + "/portrait_incredible.jpg";
            try {
                Drawable imageHero =  Drawable.createFromStream((InputStream) new URL(urlImage).getContent(),"src name");

                hero.setImage(imageHero);
            } catch (IOException e) {
                e.printStackTrace();
            }
            listheros.add(hero);
        }
        return listheros;
    }

}
