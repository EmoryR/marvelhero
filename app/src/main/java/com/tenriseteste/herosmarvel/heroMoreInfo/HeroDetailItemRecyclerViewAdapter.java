package com.tenriseteste.herosmarvel.heroMoreInfo;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import com.tenriseteste.herosmarvel.R;
import com.tenriseteste.herosmarvel.moreInfoDetails.HeroItemRecyclerViewAdapter;
import com.tenriseteste.herosmarvel.util.HeroDataClass;
import com.tenriseteste.herosmarvel.util.HeroDataListClass;

import java.util.List;

/**
 * Created by emoryfreitas on 08/07/16.
 */
public class HeroDetailItemRecyclerViewAdapter extends RecyclerView.Adapter<HeroDetailItemRecyclerViewAdapter.ViewHolder> {

    private List<HeroDataListClass> miTens;

    public HeroDetailItemRecyclerViewAdapter(List<HeroDataListClass> miTens) {
        this.miTens = miTens;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.serie_comics_stories_events_layout, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.moreList = miTens.get(position);
        holder.mTitle.setText(miTens.get(position).getTitle());
        Log.d("GetHeros", "qtd Itens:" + miTens.get(position).getList().size());
        holder.mAdapter = new HeroItemRecyclerViewAdapter(miTens.get(position).getList());
        holder.mRecycler.setAdapter(holder.mAdapter);
    }

    @Override
    public int getItemCount() {
        return miTens.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;

        public HeroDataListClass moreList;
        public final TextView mTitle;
        public RecyclerView mRecycler;
        private RecyclerView.LayoutManager mLayoutManager;
        private RecyclerView.Adapter mAdapter;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            mTitle = (TextView) itemView.findViewById(R.id.title_serie);

            mRecycler = (RecyclerView) itemView.findViewById(R.id.list_images);
            mRecycler.setHasFixedSize(true);
            // use a linear layout manager

            mLayoutManager = new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false);
            mLayoutManager.setAutoMeasureEnabled(true);
            mRecycler.setLayoutManager(mLayoutManager);

        }
    }
}
