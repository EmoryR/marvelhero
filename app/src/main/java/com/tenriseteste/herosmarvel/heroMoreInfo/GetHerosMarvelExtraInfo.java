package com.tenriseteste.herosmarvel.heroMoreInfo;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;

import com.tenriseteste.herosmarvel.HomeActivity;
import com.tenriseteste.herosmarvel.util.HeroDataClass;
import com.tenriseteste.herosmarvel.util.HeroDataListClass;
import com.tenriseteste.herosmarvel.util.MarvelConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emoryfreitas on 08/07/16.
 */
public class GetHerosMarvelExtraInfo extends AsyncTask<Void,Void,Void> {

    WeakReference<HeroMoreInfoActivity> weakHeroActivity;
    private String id;
    private String URL_MARVEL_SERIES;
    private String URL_MARVEL_COMICS;
    private String URL_MARVEL_EVENTS;
    private String URL_MARVEL_STORIES;

    List<HeroDataListClass> list = new ArrayList<HeroDataListClass>();


    public GetHerosMarvelExtraInfo(WeakReference<HeroMoreInfoActivity> weakActivity, String id) {
        this.weakHeroActivity = weakActivity;
        this.id = id;
        this.URL_MARVEL_SERIES  = "http://gateway.marvel.com/v1/public/characters/"+id +"/series?";
        this.URL_MARVEL_COMICS  = "http://gateway.marvel.com/v1/public/characters/"+id +"/comics?";
        this.URL_MARVEL_EVENTS  = "http://gateway.marvel.com/v1/public/characters/"+id +"/events?";
        this.URL_MARVEL_STORIES  = "http://gateway.marvel.com/v1/public/characters/"+id +"/stories?";
    }

    @Override
    protected Void doInBackground(Void... voids) {

        String serieUrl = MarvelConnection.getJSONMarvel(URL_MARVEL_SERIES);
        String comicsUrl = MarvelConnection.getJSONMarvel(URL_MARVEL_COMICS);
        String eventsUrl = MarvelConnection.getJSONMarvel(URL_MARVEL_EVENTS);
        //String storiesUrl = MarvelConnection.getJSONMarvel(URL_MARVEL_STORIES);

        try {
            List<HeroDataClass> serielist = parseJson(serieUrl);
            List<HeroDataClass> comicslist = parseJson(comicsUrl);
            List<HeroDataClass> eventslist = parseJson(eventsUrl);
            //List<HeroDataClass> storieslist = parseJson(storiesUrl);

            list.add(new HeroDataListClass("Series", serielist));
            list.add(new HeroDataListClass("Comics", comicslist));
            list.add(new HeroDataListClass("Events", eventslist));
            //list.add(new HeroDataListClass("Stories", storieslist));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(weakHeroActivity != null) {
            weakHeroActivity.get().update(list);
        }
    }

    public List<HeroDataClass> parseJson(String jsonReturn) throws JSONException{
        JSONObject jsonObject = (JSONObject) new JSONTokener(jsonReturn).nextValue();
        return parseResult(jsonObject.getJSONObject("data"));
    }

    public List<HeroDataClass> parseResult(JSONObject jsonObject) throws JSONException {
        List<HeroDataClass> list = new ArrayList<HeroDataClass>();
        JSONArray jsonResults = jsonObject.getJSONArray("results");
        for(int i = 0; i < jsonResults.length(); i++) {
            HeroDataClass hero = new HeroDataClass();
            JSONObject result = jsonResults.getJSONObject(i);
            String title = jsonResults.getJSONObject(i).getString("title");
            hero.setName(title);
            if (result.getJSONObject("thumbnail") != null) {
                JSONObject imageObject = result.getJSONObject("thumbnail");
                String urlImage = imageObject.getString("path") + "/portrait_small.jpg";
                try {
                    Drawable imageHero = Drawable.createFromStream((InputStream) new URL(urlImage).getContent(), "src name" + i);

                    hero.setImage(imageHero);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            list.add(hero);
        }
        return list;
    }
}
