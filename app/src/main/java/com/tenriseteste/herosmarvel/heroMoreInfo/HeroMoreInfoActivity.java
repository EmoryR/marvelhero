package com.tenriseteste.herosmarvel.heroMoreInfo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.tenriseteste.herosmarvel.R;
import com.tenriseteste.herosmarvel.util.HeroDataListClass;

import java.lang.ref.WeakReference;
import java.util.List;

public class HeroMoreInfoActivity extends AppCompatActivity {
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hero_more_info);

        String id = getIntent().getExtras().getString("id");
        String desc = getIntent().getExtras().getString("desc");

        TextView descView = (TextView) findViewById(R.id.desc_text);
        descView.setText(desc);

        byte[] byteArray = getIntent().getExtras().getByteArray("pic");

        Drawable imagebackground = new BitmapDrawable(getResources(),
                BitmapFactory.decodeByteArray(byteArray,0,byteArray.length));

        getSupportActionBar().setBackgroundDrawable(imagebackground);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setHomeButtonEnabled(true);

        GetHerosMarvelExtraInfo getHerosMarvelExtraInfo = new GetHerosMarvelExtraInfo(
                new WeakReference<HeroMoreInfoActivity>(this),id);

        getHerosMarvelExtraInfo.execute();


        mRecyclerView = (RecyclerView) findViewById(R.id.hero_data_detail_list) ;

        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    public void update (List<HeroDataListClass> list) {
            if(list != null) {
                Log.d("HeroMoreInfo", "info chegando + " + list.size());
                mAdapter = new HeroDetailItemRecyclerViewAdapter(list);
                mRecyclerView.setAdapter(mAdapter);
            } else {
                Log.d("HeroMoreInfo", "info nao esta chegando");
            }
    }


}
