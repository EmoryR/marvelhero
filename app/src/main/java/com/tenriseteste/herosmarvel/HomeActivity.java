package com.tenriseteste.herosmarvel;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.tenriseteste.herosmarvel.heroMoreInfo.HeroMoreInfoActivity;

import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;
import java.util.List;

public class HomeActivity extends AppCompatActivity  {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        GetHerosMarvelInfo getHeros = new GetHerosMarvelInfo(this, new WeakReference<HomeActivity>(this));
        getHeros.execute();


        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)

    }

    public void updateView(List<HerosMarvelItem> heros) {

        mAdapter = new MyHeroItemRecyclerViewAdapter(heros, new MyHeroItemRecyclerViewAdapter.OnHeroItemClicked() {
            @Override
            public void OnHeroInteraction(HerosMarvelItem hero) {
                Intent intent = new Intent(getApplicationContext(), HeroMoreInfoActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("id",hero.getIdCharacter());
                bundle.putString("desc", hero.getDescricao());

                Bitmap bitmap = ((BitmapDrawable)hero.getImage()).getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] bitmapdata = stream.toByteArray();

                bundle.putByteArray("pic",bitmapdata);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
    }

}
