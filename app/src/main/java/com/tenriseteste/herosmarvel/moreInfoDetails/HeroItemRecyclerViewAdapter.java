package com.tenriseteste.herosmarvel.moreInfoDetails;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tenriseteste.herosmarvel.HerosMarvelItem;
import com.tenriseteste.herosmarvel.R;
import com.tenriseteste.herosmarvel.util.HeroDataClass;
import com.tenriseteste.herosmarvel.util.HeroDataListClass;
import com.tenriseteste.herosmarvel.util.HeroMarvelDetailItem;

import java.util.List;

/**
 * Created by emoryfreitas on 08/07/16.
 */
public class HeroItemRecyclerViewAdapter extends RecyclerView.Adapter<HeroItemRecyclerViewAdapter.ViewHolder> {

    List<HeroDataClass> mItens;

    public HeroItemRecyclerViewAdapter(List<HeroDataClass> mItens) {
        this.mItens = mItens;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_text_detail,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.details = mItens.get(position);
        holder.mDescription.setText(mItens.get(position).getName());
        holder.mImage.setImageDrawable(mItens.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return mItens.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView mDescription;
        public HeroDataClass details;
        public final ImageView mImage;

        public ViewHolder(View itemView) {
            super(itemView);
            this.mView = itemView;
            mDescription = (TextView) itemView.findViewById(R.id.image_description);
            mImage = (ImageView) itemView.findViewById(R.id.image_detail);
        }
    }
}
