package com.tenriseteste.herosmarvel.util;

import java.util.List;

/**
 * Created by emoryfreitas on 08/07/16.
 */
public class HeroDataListClass {
    private String title;
    private List<HeroDataClass> list;

    public HeroDataListClass(String title, List<HeroDataClass> list) {
        this.title = title;
        this.list = list;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<HeroDataClass> getList() {
        return list;
    }

    public void setList(List<HeroDataClass> list) {
        this.list = list;
    }
}
