package com.tenriseteste.herosmarvel.util;

import android.graphics.drawable.Drawable;

/**
 * Created by emoryfreitas on 08/07/16.
 */
public class HeroDataClass {
    private Drawable image;

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
}
