package com.tenriseteste.herosmarvel.util;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by emoryfreitas on 08/07/16.
 */
public class MarvelConnection {

    //    private ProgressDialog Dialog = new ProgressDialog(mContext);
    private final String URL_MARVEL = "http://gateway.marvel.com/v1/public/characters?";
    private static final String myPublicKey = "8630dc1b317dc86da29f1db435e4ea07";
    private static final String myPrivatekey = "351da5058a0741194706170823759ced3b84d5df";


    public static String geraTs() {
        Long tsLong = System.currentTimeMillis() / 1000;
        return tsLong.toString();
    }

    public static String geraMd5(String ts) {
        String input = ts + myPrivatekey + myPublicKey;
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        digest.update(input.getBytes());
        byte messageDigest[] = digest.digest();
        StringBuilder hexString = new StringBuilder();
        for (byte aMessageDigest : messageDigest) {
            String h = Integer.toHexString(0xFF & aMessageDigest);
            while (h.length() < 2)
                h = "0" + h;
            hexString.append(h);
        }

        return hexString.toString();
    }

    public static String getJSONMarvel(String url_marvel) {
        try {
            //Get timestamp
            String ts = geraTs();
            String md5Hash = geraMd5(ts);
            //Generate md5 hash

            //Get info from API
            URL url = new URL(url_marvel + "apikey=" + myPublicKey + "&ts=" + ts + "&hash=" + md5Hash);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                return stringBuilder.toString();

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                Log.d("Heros", "close connection");
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            Log.e("ERROR", e.getMessage(), e);
        }
        return null;
    }
}
