package com.tenriseteste.herosmarvel.util;

import android.graphics.drawable.Drawable;

/**
 * Created by emoryfreitas on 08/07/16.
 */
public class HeroMarvelDetailItem {

    private Drawable image;
    private String description;

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
